<?php
	include('include/verifCon.php');
	
	include('include/openbdd.php');
	$req = $bdd->prepare('SELECT seenapps FROM utilisateurs WHERE user = ?');
	$req->execute(array($_SESSION['user']));
	$user = $req->fetch();
	if ($user != null)
	{
		$seenapps = $user['seenapps'];
		if (strpos($seenapps, '#'.$_GET['l'].'#') === false)
		{
			$seenapps = $seenapps.$_GET['l'].'#';
			$req = $bdd->prepare('UPDATE utilisateurs SET seenapps = ? WHERE user = ?');
			$req->execute(array($seenapps, $_SESSION['user']));
		}
	}
	else
	{
		$req = $bdd->prepare('INSERT INTO utilisateurs(user,seenapps) VALUES(?,?)');
		$seenapps = '#'.$_GET['l'].'#';
		$req->execute(array($_SESSION['user'], $seenapps));
	}
?>
<!DOCTYPE html>
<?php
	$couleur="gris";
 
?>

<html id="gris"><head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Eistiens.work</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php
		
		include("include/header.php");
		

	?>
	<section>
		<?php
		//connection ssh
		$connection = ssh2_connect($serveur, 22);
	    	ssh2_auth_password($connection, $_SESSION["user"],$_SESSION["pwd"] );
	    	//envoie de la commande pour recuperer les infos
		$com = fix(syst($connection,'~maisonneuv/launcher -i -w '.$_GET['l']));
		//fermeture de la connection
		ssh2_exec($connection, "exit");
		unset($connection);

		/// Traitement de la reponse
	        $com = trim($com);	
		$out = explode('|',$com);
		//echo "-".$out[0]."-";
		if($out[0] !== "exit")
		{   	
			?>
			<section id="info">

				<h1 id="name"><?php echo $out[0] ?></h1>

				<?php 
					$maversion = intval($out[2]);
				    
					$version = intval($out[1]);

					if($maversion == $version)//version à jour
					    $install = 0;
					else if($maversion == 0)//pas installé
					    $install = 1;
					else//sinon pas à jour
					    $install = 2;

					switch($install)
					{
						case 0:
							echo '<div id="version" style="color:#004500"> A jour </div>'; 
						break;
						case 1:
							echo '<div id="version" style="color:#a60910"> Non installé </div>';
						break;
						case 2:
							echo '<div id="version" style="color:#5b4304">Version Obsolète</div>';				
						break;
						case 3:
							echo '<div id="version" style="color:#04255b"> Une erreur est survenu... </div>';				
						break;
					}
				?>
				
					<?php 
						switch($install)
						{
							case 0:
								echo "<a href=\"install.php?l=".$_GET['l']."\" id='install' style='background-color:#163c76'>Réinstaller</a>";
							break;
							case 1:
								echo "<a href=\"install.php?l=".$_GET['l']."\" id='install' style='background-color:green'>Installer</a>";
							break;
							case 2:
								echo "<a href=\"install.php?l=".$_GET['l']."\" id='install' style='background-color:#5f075a'>Mettre à jour</a>";
							break;
							default:
								echo "Tenter l'installation";
							
						}
					?>
				</a>
				<br/>


				

				<article>
					<h1>Description</h1>
					<p><?php echo $out[3]; ?></p>
				</article>
			</section>
			<img src="images/<?php echo $out[4]; ?>" id="info-img"></div>
		<?php
		}
else
{
	echo "<p>Erreur 404, le programme n'existe pas</p>";
	echo "<a href='index.php'>Retourner à l'acceuil...</a>";
}?>	
	</section>
</body>
</html>
