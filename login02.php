<?php session_start();

include('include/utils.php');

/*	Utilisation de Selenium X GitLab pour verifier les identitées
include('include/connectSelenium.php');
include('include/testSelenium.php');
*/

/*	Utilisation de l'API auth.eistiens.net pour la verif des identitées	*/
include('include/checkCredential.php');


if (isset($_POST['user']) && isset($_POST['mdp']) )
{
	if(checkCredential($_POST['user'],$_POST['mdp']))
	{
		 $_SESSION['user'] = $_POST['user'];
		 $_SESSION['pwd'] = $_POST['mdp'];
		 header("Location: index.php");
		 exit;
		 echo "yes";
	}
	else
	{
		header("Location:login.php?erreur=login");
		echo "none";
	}

}
else
{
	header("Location:login.php?erreur=champ");
}
?>
