Interface web pour Pancakes/LauncherV2
======================================

![Login weblauncher](http://oi63.tinypic.com/6pnpyp.jpg)
![Acceuil weblauncher](http://oi63.tinypic.com/106y2ax.jpg)
![Hover app](http://oi68.tinypic.com/jv3ubk.png)
![Non installé app info](http://oi66.tinypic.com/2hncc5z.jpg)
![Update app info](http://i66.tinypic.com/iqcuf4.png)
![Resposive design acceuil](http://dunnnk.com/share/1461156930/Mockup-Generated-by-Dunnnk.jpg)

![Responsive design apps](http://magicmockups.com/media/screen/guest/03/397cdc400917485cb5744882924fb398_5_1600.jpg)


Présentation :
--------------

WebLauncher est un site web php qui permet de controler Pancakes/LauncherV2 sur
un serveur ssh.


Il permet d'installer les application, les mettre à jour, les désinstaller,
et consulter les information les concernant.

Utilisation / démonstration :
-----------------------------
Vous pouvez voir le site à l'addresse http://eistiens.work

Contribution :
--------------
Vous pouvez nous faire une suggestion ou nous signaler un bug en creant
une issue, cela nous aiderais beaucoup !

License :
---------
WebLauncher est disponible sous license GPLv3.