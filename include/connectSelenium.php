<?php
 namespace Facebook\WebDriver;
        use Facebook\WebDriver\Remote\DesiredCapabilities;
        use Facebook\WebDriver\Remote\RemoteWebDriver;
        require_once('/opt/vendor/autoload.php');

        $host = 'http://localhost:4444/wd/hub';
        $capabilities = DesiredCapabilities::htmlunit();
        $capabilities->setCapability('javascriptEnabled', false);
        $driver = RemoteWebDriver::create($host, $capabilities, 5000);
?>
