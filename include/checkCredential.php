<?php

function checkCredential ($user, $pass) 
{
	$url = 'http://auth.eistiens.net';
	
	$curl = curl_init();

	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_COOKIESESSION, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);

	$postfield = array(
		'username' => $user,
		'password' => $pass,
	);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postfield);
	
	$return = curl_exec($curl);
	curl_close($curl);

	return (! empty($return));
}

?>
