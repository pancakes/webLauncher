<?php
session_start();
if (!isset($_SESSION['user']) || !isset($_SESSION['pwd']))
{
	header('Location:login.php');
	exit;
}

include('include/var.php');
include('include/utils.php');

?>
<!DOCTYPE html>
<html id="bleu"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Eistiens.work</title>
  <meta charset="utf-8">
  <script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php

	include("include/header.php");
	      	
	 ?>
	
	<section id="applis">
		<SELECT NAME="type" id="type">
			<OPTION VALUE="tous">tous</OPTION>
			<OPTION VALUE="jeu">Jeux</OPTION>
			<OPTION VALUE="logiciel">Logiciel</OPTION>
			<OPTION VALUE="utilitaire">Utilitaire</OPTION>
		</SELECT>
		<br/>
		<br/>

		<?php
			//connection ssh
		$connection = ssh2_connect($serveur, 22);
	    	ssh2_auth_password($connection, $_SESSION["user"],$_SESSION["pwd"] );
	    	//envoie de la commande pour recuperer les infos
	    	$out = fix(syst($connection,'~maisonneuv/launcher -w -l'));
		ssh2_exec($connection, "exit");
		unset($connection);
		


		$out = explode('|',$out);
		 //print_r ($out); 	
		//$out = explode("|", "test@5@jeu|superjeu@7@jeu|sublime@3@logiciel|tor@1@logiciel|minecraft@1@jeu|geany@1@logiciel|hmm@3@jeu|2048@1@jeu");
	    	

		include("include/openbdd.php");	// Connection à la BDD

		$req = $bdd->prepare('SELECT seenapps FROM utilisateurs WHERE user = ?');
		$req->execute(array($_SESSION["user"]));
		$appseen = $req->fetch();
		for($i = 0; $i<= count($out)-1;$i++)
	    	{
							    		
	    		$donnes = explode('@',$out[$i]);
	    		if($donnes[1] == $donnes[6])
			{
				$etat = 'A jour';
				$etatColor = 'green';
			}
			else if($donnes[6] == '0')
			{
				$etat = 'Non installé';
				$etatColor = 'red';
			}
			else
			{
				$etat = 'Version obsolète';
				$etatColor = '#d007ac';
			}

			echo "<a href='info.php?l=$donnes[0]' class='$donnes[2]'>";
			
			if(strpos($appseen['seenapps'], "#".$donnes[0]."#") === false)
			{
	    			echo "<div class='app new' id='$donnes[0]' style='background-image:url(\"images/new.png\"),url(\"images/$donnes[3]\");'>";
			}
			else
			{
	    			echo "<div class='app' id='$donnes[0]' style='background-image:url(\"images/$donnes[3]\");'>";
			}
	    		echo "<h1>$donnes[5]</h1>";
	    		echo "<div class='instal' style='color:$etatColor'>$etat</div>";
			echo '</div>';

			echo '</a>';
	    	}	



    	?>
		
	</section>

<script>
		$("#type").change(function() {
			affiche ($("#type option:selected").text());
		});

		function affiche(selection)
		{
			$(".logiciel, .utilitaire, .jeu").fadeOut(200);
			console.log(selection);
			if(selection == "Jeux")
			{
				$(".jeu").delay(200).fadeIn(200);
			}
			else if(selection == "Logiciel")
			{
				$(".logiciel").delay(200).fadeIn(200);
			}
			else if(selection == "Utilitaire")
			{
				$(".utilitaire").delay(200).fadeIn(200);
			}
			else
			{
				$(".logiciel, .jeu, .utilitaire").delay(200).fadeIn();
			}
		}
		
	</script>


</body>
</html>
